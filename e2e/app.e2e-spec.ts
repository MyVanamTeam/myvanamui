import { LitewebAppPage } from './app.po';

describe('liteweb-app App', () => {
  let page: LitewebAppPage;

  beforeEach(() => {
    page = new LitewebAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
