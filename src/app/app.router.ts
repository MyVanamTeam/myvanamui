import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { SearchNameFormComponent } from './search-name-form.component';
import { FavoritesComponent } from './favorites.component';
import { ComboNamesComponent } from './combonames.component';
import { SuggestComponent } from './suggest.component';
import { TestimonialsComponent } from './testimonials.component';
import { TwinNamesComponent } from './twinnames.component';

// Footer Block Components
import { AboutUsComponent } from './about-us.component';
import { DisclaimerComponent } from './disclaimer.component';
import { ContactUsComponent } from './contact-us.component';
import { FAQComponent } from './faq.component';
import { ContributeComponent } from './contribute.component';

export const router : Routes = [
    {path:'', component:SearchNameFormComponent},
    {path:'favorites', component:FavoritesComponent},
    {path:'twinnames', component:TwinNamesComponent},
    {path:'testimonials', component:TestimonialsComponent},
    {path:'combonames', component:ComboNamesComponent},
    {path:'suggest', component:SuggestComponent},
    {path:'aboutus',component:AboutUsComponent},
    {path:'disclaimer',component:DisclaimerComponent},
    {path:'contactus',component:ContactUsComponent},
    {path:'faq',component:FAQComponent},
    {path:'contribute',component:ContributeComponent}
];

export const routes : ModuleWithProviders = RouterModule.forRoot(router);