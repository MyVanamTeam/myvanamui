import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarunComponent } from './tarun.component';

describe('TarunComponent', () => {
  let component: TarunComponent;
  let fixture: ComponentFixture<TarunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
