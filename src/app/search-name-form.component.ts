import {Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormParamsService } from './formparams.service';
import { SearchResultService } from './searchresult.service';

@Component({
    selector: 'app-nm-search-name-form',
    templateUrl: './search-name-form.component.html',
    styleUrls: ['./search-name-form.component.css']
})

export class SearchNameFormComponent implements OnInit {
    searchform;
    resultData;
    constructor(private formBuilder: FormBuilder,
                private formParams: FormParamsService,
                public searchResult: SearchResultService) {}
    ngOnInit() {
        this.searchform = this.formBuilder.group({
            gender: this.formBuilder.control(''),
            theme: this.formBuilder.control(''),
            language: this.formBuilder.control(''),
            contains: this.formBuilder.control('')
        });
    }
}
