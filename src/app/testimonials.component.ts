/**
 * Created by vikramreddy on 5/1/2017.
 */
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TestimonialService } from './testimonials.service';

@Component({
  selector: 'app-nm-testimonials',
  // template: `<h4 style="text-align:center;">Testimonials Page comes here...</h4>`
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})

export class TestimonialsComponent {

  insertStatus : string;

      resultData: Observable<Array<any>>;

      constructor(private testResult: TestimonialService) {
          // alert('Entered Constructor');

          this.testResult.Result.subscribe(
              res => this.resultData = res,
              err => console.log(err)
          );
          // alert('value of resultData: '+this.resultData);
          // alert('value of testResult: '+this.testResult);
          // alert('Exiting Constructor');
      }

      ngOnInit(){
        // alert('Entered ngOnInit()');
        this.testResult.getResult();
        // alert('Exiting ngOnInit()');
      }

      onSubmit(value){
          // alert(value.submitter+'  '+value.email+'   '+value.message);
          // alert(JSON.stringify(value));
          console.log(value);
          this.testResult.insertTestimonial(value)
              .subscribe(
                  res => this.insertStatus = (res.toString()=='true')?
                  'Hey '+value.submitter+', Thank You for Submitting Testimonial !!':
                  'Sorry, Your Testimonials is not Submitted !!'
              );
      }

}
