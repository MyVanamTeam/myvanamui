/**
 * Created by vikramreddy on 5/1/2017.
 */
import { Component } from '@angular/core';
import { SuggestService } from './suggest.service';

@Component({
  selector: 'app-nm-suggest',
  templateUrl: './suggest.component.html',
  styleUrls: ['./suggest.component.css'],
  providers: [SuggestService]
})

export class SuggestComponent{
  namenf_ : string;
  insertResult : {'status':string};
  status: string;
  insertRes : string;

  constructor(private _suggestService : SuggestService){
  }

  onSubmit(value : any){
    // alert('onSubmit() entered...');
    this.namenf_ = value.nameString;
    // alert('NAME: '+this.namenf_);
    // alert(this._suggestService.insertName());
    // this.insertResult = this._suggestService.insertName(this.namenf_);
    // alert('RESULT: '+this.insertResult.status);
    // status = this.insertResult.status;
    // this.insertRes = this._suggestService.insertName(this.namenf_)
    this._suggestService.insertName(this.namenf_)
                      .subscribe(res => this.insertRes = 
                        (res.toString()=='true')?
                          'Thank you for suggesting '+this.namenf_+' !!':
                          'Sorry, '+this.namenf_+' is a Duplicate !!' 
                        ) ;
    // alert('RESULT: '+this.insertRes);
    console.log(this.insertRes);
    // alert('onSubmit() exiting...');
  }
 
}
