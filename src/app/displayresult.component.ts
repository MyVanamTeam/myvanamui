import { Component } from '@angular/core';
import { SearchResultService } from './searchresult.service';
import { FormParamsService} from './formparams.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-nm-display-result',
    templateUrl: './displayresult.component.html',
    styleUrls: ['./displayresult.component.css']
})

export class DisplayResultComponent {
    resultData: Observable<Array<any>>;
    favItem = {'name':'','meaning':'','gender':''};

    gender: string;
    contains: string;
    constructor(private searchResult: SearchResultService,
                private formParams: FormParamsService) {

        this.searchResult.Result.subscribe(
            res => this.resultData = res,
            err => console.log(err)
        );
    }

    addToFavs(nameVal,meaningVal,genderVal){
        // alert('Add to Favs clicked !!'+nameVal);

        if(typeof(Storage) !== "undefined") {
            if (localStorage.favsList !== undefined) {
                this.favItem.name = nameVal;
                this.favItem.meaning = meaningVal;
                this.favItem.gender = genderVal;
                // localStorage.favsList = localStorage.favsList + ','+nameVal;
                localStorage.favsList = localStorage.favsList + ','+this.favItem;
                // alert('localStorage.favsList - '+JSON.parse(localStorage.favsList));
                alert('this.favItem - '+JSON.stringify(this.favItem));
            } else {
                // localStorage.favsList = nameVal;
                localStorage.favsList = this.favItem;
            }
            // document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s) in this session.";
            // alert(nameVal+' has been added to favsList: '+localStorage.favsList);
        } else {
            // document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
            alert('Sorry, your browser does not support web storage...');
        }
    }
}
