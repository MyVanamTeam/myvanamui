import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TwinNamesService{

    // private _url : string = 'http://localhost:8080/NamakaranamWP/name/findNameService';
    // public url : string = 'http://ec2-34-223-237-229.us-west-2.compute.amazonaws.com/NamakaranamWP/name/findNameService';
    // public url : string = 'http://namakaranam.com/NamakaranamWP/name/findTwinNameService';
    public url : string = 'http://ec2-18-222-212-123.us-east-2.compute.amazonaws.com:8080/NamakaranamWP/name/findNameService';

    constructor(private _http : Http){
    }

    getTwinNames(formValues){
        // alert('ComboNamesService.getComboNames() called.....: '+JSON.stringify(formValues));
        return this._http.post(this.url,formValues)
            .map( (response : Response) => response.json() );
    }
}