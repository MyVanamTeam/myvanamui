import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SuggestService {

    // private _url : string = 'http://localhost:8080/NamakaranamWP/name/postNameNFService';
    // public url : string = 'http://ec2-34-223-237-229.us-west-2.compute.amazonaws.com/NamakaranamWP/name/postNameNFService';
    // public url : string = 'http://namakaranam.com/NamakaranamWP/name/putNameNFService';
    public url : string = 'http://ec2-18-222-212-123.us-east-2.compute.amazonaws.com:8080/NamakaranamWP/name/postNameNFService';

    nameJSON : {'name':string};

    constructor(private _http : Http) {}
    
    insertName(nameVal){
        // return {'status':'inserting '+nameVal+' is success !!'};
        // return true;
        // alert('SuggestService.insertName() called...');
        // alert(nameVal);
        this.nameJSON = {'name':nameVal};
        // alert('SuggestService.... nameJSON '+JSON.stringify(this.nameJSON));
        return this._http.put(this.url, this.nameJSON)
                .map( (response : Response) => response.text());
    }

}
