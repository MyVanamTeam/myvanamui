/**
 * Created by vikramreddy on 5/1/2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-nm-favorites',
  templateUrl: './favorites.component.html'
})

export class FavoritesComponent {

    favsList : string[] ;
    favsListLength : number;
    favsListJSON = [{'name':'','meaning':'','gender':''}];
    favsListOriginal : string;

    /* Declaring a JSON object that holds favItems as an Array */
    favItem : {'name':'','meaning':'','gender':''};
    i : number;
    // favItems : favItem[];

  /* 
  getFavsList(){
    if(typeof(Storage) !== "undefined") {
        if (localStorage.favsList !== undefined) {
            // localStorage.favsList = localStorage.favsList + ';'+nameVal;
            document.getElementById("favsList").innerHTML = "Favs List: "+localStorage.favsList;
        } else {
            // localStorage.favsList = nameVal;
            document.getElementById("favsList").innerHTML = "Your Favs List is Emtpy";
        }
        // document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s) in this session.";
        // alert(nameVal+' has been added to favsList: '+localStorage.favsList);
    } else {
        // document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
        alert('Sorry, your browser does not support web storage...');
    }
  }
  */

  ngOnInit(){
    if(typeof(Storage) !== "undefined") {
        if (localStorage.favsList !== undefined) {
            // localStorage.favsList = localStorage.favsList + ';'+nameVal;
            // document.getElementById("favsList").innerHTML = "Favs List: "+localStorage.favsList;
            this.favsList = JSON.stringify(localStorage.favsList).split(",");
            this.favsListLength = this.favsList.length;
            this.favsListOriginal = localStorage.favsList;
            this.favsListJSON = localStorage.favsList;
            // alert(this.favsListJSON.length);
            for(this.i = 0 ; this.i < 4; this.i++){
                // alert(JSON.stringify(this.favsListJSON[this.i]));
            }
            // alert('No. of names in favList array: '+this.favsList.length);
            // alert(this.favsList.indexOf('CHARUPRABHA'));
        } else {
            // localStorage.favsList = nameVal;
            // document.getElementById("favsList").innerHTML = "Your Favs List is Emtpy";
            this.favsList = [''];
        }
        // document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s) in this session.";
        // alert(nameVal+' has been added to favsList: '+localStorage.favsList);
    } else {
        // document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
        alert('Sorry, your browser does not support web storage...');
    }
  }

  clearFavsList(){
    if(typeof(Storage) !== "undefined") {
        if (localStorage.favsList !== undefined) {
            // localStorage.favsList = localStorage.favsList + ';'+nameVal;
            // document.getElementById("favsList").innerHTML = "Favs List: "+localStorage.favsList;
            localStorage.removeItem("favsList");
            this.favsList = [''];
            // document.getElementById("favsList").innerHTML = "Your Favs List is Emtpy";
        } else {
            // localStorage.favsList = nameVal;
            // document.getElementById("favsList").innerHTML = "Your Favs List is Emtpy Already !!";
            this.favsList = [''];
        }
        // document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s) in this session.";
        // alert(nameVal+' has been added to favsList: '+localStorage.favsList);
    } else {
        // document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
        alert('Sorry, your browser does not support web storage...');
    }
  }

  removeFavItem(removeName){
    if(typeof(Storage) !== "undefined"){
        if(localStorage.favsList !== undefined){
            alert('removeFavItem() called with parameter: '+removeName);

            localStorage.favsList = localStorage.favsList.replace(removeName+';','');

            this.favsList.splice(this.favsList.indexOf(removeName),1);
            localStorage.favsList = this.favsList;
        }
        else{
            alert('Your Favorites list does not exist');
        }
    }
    else{
        alert('Sorry, your browser does not support web storage...');
    }
  }

}
