import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class TestimonialService {
    // public url : string = 'http://ec2-34-223-237-229.us-west-2.compute.amazonaws.com/NamakaranamWP/name/postTestService';
    // public url : string = 'http://namakaranam.com/NamakaranamWP/name/postTestService';
    // public url : string = 'http://namakaranam.com/';
    public url : string = 'http://ec2-18-222-212-123.us-east-2.compute.amazonaws.com:8080/';
    
    public Result = new Subject<any>();
    constructor(private http: Http) {}
    public getResult() {
        // alert('TestimonialService.getResult() called');
        const json = JSON.stringify({'a':'b'});
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        // this.Result.next(this.http.post('http://localhost:8080/NamakaranamWP/name/postTestService', json, options)
        //                .map(res => res.json()));
        this.Result.next(this.http.post(this.url+'NamakaranamWP/name/postTestService', json, options)
                        .map(res => res.json()));
    }

    insertTestimonial(value:any){
        // alert('Inside TestimonialService.insertTestimonial(): ...'+JSON.stringify(value));
        return this.http.put(this.url+'NamakaranamWP/name/putTestService', value)
                    .map( (response : Response) => response.text() );
    }
}
