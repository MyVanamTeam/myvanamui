/**
 * Created by vikramreddy on 5/1/2017.
 */
import { Component } from '@angular/core';
import { TwinNamesService } from './twinnames.service';

@Component({
  selector: 'app-nm-twin-names',
  templateUrl: './twinnames.component.html',
  providers: [TwinNamesService] 
})

export class TwinNamesComponent {

    resultData = [
      {'name1':'','meaning1':'','gender1':'','name2':'','meaning2':'','gender2':''}
    ];

    constructor(private _twinNamesService : TwinNamesService){
    }

  onSubmit(formValues){
    // alert('TwinNames submitted....'+JSON.stringify(formValues));
        this._twinNamesService.getTwinNames(formValues)
             .subscribe(res => this.resultData = res);
  }
}
