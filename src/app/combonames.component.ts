/**
 * Created by vikramreddy on 5/1/2017.
 */
import { Component } from '@angular/core';
import { ComboNamesService } from './combonames.service';

@Component({
  selector: 'app-nm-combo-names',
  templateUrl: './combonames.component.html',
  providers: [ComboNamesService] 
})

export class ComboNamesComponent {

  resultData = [
    {'name':'','meaning':'','gender':''}
  ];

  resultDataLength : number;

  constructor(private _comboNamesService : ComboNamesService){
  }

  onSubmit(formValues){
    // alert(JSON.stringify(formValues));
    this._comboNamesService.getComboNames(formValues)
             .subscribe(res => this.resultData = res);
    
    this.resultDataLength = this.resultData.length;
  }

      addToFavs(nameVal){
        // alert('Add to Favs clicked !!'+nameVal);

        if(typeof(Storage) !== "undefined") {
            if (localStorage.favsList !== undefined) {
                localStorage.favsList = localStorage.favsList + ','+nameVal;
            } else {
                localStorage.favsList = nameVal;
            }
            // document.getElementById("result").innerHTML = "You have clicked the button " + localStorage.clickcount + " time(s) in this session.";
            // alert(nameVal+' has been added to favsList: '+localStorage.favsList);
        } else {
            // document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
            alert('Sorry, your browser does not support web storage...');
        }
      }
}
