import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Ng2PaginationModule} from 'ng2-pagination';
import { FormsModule } from '@angular/forms';

import { routes } from './app.router';

import { AppComponent } from './app.component';
import { NavComponent } from './nav.component';
import { DisplayResultComponent } from './displayresult.component';
import { SearchNameFormComponent } from './search-name-form.component';
import { ComboNamesComponent } from './combonames.component';
import { FavoritesComponent } from './favorites.component';
import { SuggestComponent } from './suggest.component';
import { TestimonialsComponent } from './testimonials.component';
import { TwinNamesComponent } from './twinnames.component';

// Footer Block Components
import { AboutUsComponent } from './about-us.component';
import { DisclaimerComponent } from './disclaimer.component';
import { ContactUsComponent } from './contact-us.component';
import { FAQComponent } from './faq.component';
import { ContributeComponent } from './contribute.component';

// Services
import { SearchResultService } from './searchresult.service';
import { FormParamsService } from './formparams.service';
import { TestimonialService } from './testimonials.service';
import { SuggestService } from './suggest.service';
import { TarunComponent } from './tarun/tarun.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DisplayResultComponent,
    SearchNameFormComponent,
    ComboNamesComponent,
    FavoritesComponent,
    SuggestComponent,
    TestimonialsComponent,
    TwinNamesComponent,

    AboutUsComponent,
    DisclaimerComponent,
    ContactUsComponent,
    FAQComponent,
    ContributeComponent,
    TarunComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    Ng2PaginationModule,
    routes,
    FormsModule
  ],
  providers: [SearchResultService, FormParamsService, TestimonialService, SuggestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
