import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class SearchResultService {
    public Result = new Subject<any>();
    // public url : string = 'http://ec2-35-167-67-20.us-west-2.compute.amazonaws.com:8080/NamakaranamWP/name/postNameService';
    // public url : string = 'http://namakaranam.com/NamakaranamWP/name/postNameService';
    public url : string = 'http://ec2-18-222-212-123.us-east-2.compute.amazonaws.com:8080/NamakaranamWP/name/postNameService';

    constructor(private http: Http) {}
    public getResult(params) {
        const json = JSON.stringify({   contains:   params.contains, 
                                        gender:     params.gender, 
                                        theme:      params.theme,
                                        language:   params.language
                                    });
        // alert('Inside SearchResultService.getResult()......'+JSON.stringify(params));
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        // return this.http.post('http://localhost:8080/NamakaranamWP/name/postNameService', json, options)
        //                 .map(res => res.json());
        // this.Result.next(this.http.post('http://localhost:8080/NamakaranamWP/name/postNameService', json, options)
        //                .map(res => res.json()));
        
        this.Result.next(this.http.post(this.url, json, options)
                        .map(res => res.json()));
    
    }
}
